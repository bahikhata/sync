
const jwt = require('jsonwebtoken');
const secret = process.env.SECRET;

var verify = () => {
    return (req, res, next) => {
      let token = req.headers['x-access-token'] || req.headers['authorization'];
      if(token){
        if (token.startsWith('Bearer ')) {
         // Remove Bearer from string
         token = token.slice(7, token.length);
        }
        jwt.verify(token, secret, (err, decoded) => {
          if (err) {
            if(err.name == "TokenExpiredError"){
              return res.json({
                success: false,
                message: 'Token Expired. Re-Login.'
              });
            }
            return res.json({
              success: false,
              message: 'Authentication Failed'
            });
          } else {
            req.decoded = decoded;
            next();
          }
        });
      } else {
        return res.json({
          success: false,
          message: 'Authentication Failed'
        });
      }
    }
}

var sign = (username) => {
  return jwt.sign({username: username}, secret, { expiresIn: '24h' });
}

module.exports = {
  verify,
  sign
}
