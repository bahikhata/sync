const fs = require('fs');
const path = require('path');
const AWS = require('aws-sdk');

var s3 = new AWS.S3();
AWS.config.update({
  region: 'ap-south-1',
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_ACCESS_KEY_ID
  }
});

upload = (file) => {
    return new Promise((resolve, reject) => {
      var bucketName = process.env.S3_Bucket;
      //s3.createBucket({Bucket: bucketName}, function(){
      var params = {Bucket: bucketName, Key: file.originalname, Body: fs.createReadStream(file.path)};
      const url = "https://" + bucketName + ".s3.ap-south-1.amazonaws.com/" + file.originalname;
      s3.putObject(params, (err, data) => {
        if(!err)
          resolve(url);
        else
          reject(err);
      });
    });
}

module.exports = {
  upload
};
