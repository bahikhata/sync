var token = require('./token');
var mail = require('./mail');
var s3 = require('./s3');

module.exports = {
  token,
  mail,
  s3
};
