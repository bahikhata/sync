
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

sendMail = (sendTo, templateId, options) => {
  const message = {
    to: sendTo,
    from: process.env.MAIL_SENDER,
    templateId: templateId,
    dynamic_template_data: options
  };
  sgMail.send(message);
}

module.exports = {
  sendMail
};
