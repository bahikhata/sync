
const Sequelize = require('sequelize');
let DB_URI;
if(process.env.ENV_TYPE == "DEV"){
  DB_URI = process.env.DB_URI;
} else if(process.env.ENV_TYPE == "STAGE"){
  DB_URI = process.env.DB_URI_STAGE;
} else if(process.env.ENV_TYPE == "PROD"){
  DB_URI = process.env.DB_URI_PROD;
}
const sequelize = new Sequelize(DB_URI, {
  logging: false
});

const {
  UserSchema,
  BrandSchema,
  ResellerSchema,
  TransactionSchema,
  BankSchema,
  AccountSchema,
  LoanSchema,
  LandSchema
} = require('./Schema');

const User = sequelize.define('user', UserSchema);
const Brand = sequelize.define('brand', BrandSchema);
const Reseller = sequelize.define('reseller', ResellerSchema);
const Transaction = sequelize.define('transaction', TransactionSchema);
const Bank = sequelize.define('bank', BankSchema);
const Account = sequelize.define('account', AccountSchema);
const Loan = sequelize.define('loan', LoanSchema);
const Land = sequelize.define('land', LandSchema);

User.hasMany(Transaction);
User.hasMany(Account);
User.hasMany(Bank);
Account.hasOne(Transaction);
Bank.hasOne(Transaction);

User.sync();//{force: true});
Brand.sync();//{force: true});
Reseller.sync();
Transaction.sync();
Bank.sync();
Account.sync();
Loan.sync();
Land.sync();

module.exports = {
  User,
  Reseller,
  Brand,
  Transaction,
  Bank,
  Account,
  Loan,
  Land
}
