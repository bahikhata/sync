const Sequelize = require('sequelize');

const SmeSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  name: {
   type: Sequelize.STRING
  },
  period: {
    type: Sequelize.DATE
  },
  phone: {
    type: Sequelize.STRING(15),
    allowNull: false
  },
  syncState: {
     type: Sequelize.INTEGER,
     defaultValue: 1
  }
};

const TransactionSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  amount: {
    type: Sequelize.INTEGER
  },
  detail: {
    type: Sequelize.TEXT
  },
  media: {
    type: Sequelize.STRING
  },
  cancelled: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  syncState: {
    type: Sequelize.INTEGER,
    defaultValue: 1
  }
};

const AccountSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING
  },
  phone: {
    type: Sequelize.STRING
  },
  amount: {
    type: Sequelize.DOUBLE
  },
  cancelled: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  syncState: {
    type: Sequelize.INTEGER,
    defaultValue: 1
  }
}

const BankSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING
  },
  detail: {
    type: Sequelize.JSON
  },
  amount: {
    type: Sequelize.DOUBLE
  },
  syncState: {
    type: Sequelize.INTEGER,
    defaultValue: 1
  }
}

const BrandSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  email: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true
  },
  password: {
    allowNull: false,
    type: Sequelize.STRING
  },
  emailVerified: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  token: {
    type: Sequelize.STRING
  },
  keyContact: {
    type: Sequelize.JSON
  }
}

const LoanSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  business: {
    type: Sequelize.JSON
  },
  amount: {
    type: Sequelize.DOUBLE
  },
  productChoice: {
    type: Sequelize.STRING
  },
  documents: {
    type: Sequelize.ARRAY(Sequelize.STRING)
  },
  status: {
    type: Sequelize.INTEGER
  },
  userId: {
    type: Sequelize.UUID
  }
}

const LandSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  location: {
    type: Sequelize.STRING
  },
  name: {
    type: Sequelize.STRING
  },
  area: {
    type: Sequelize.INTEGER
  },
  media: {
    type: Sequelize.STRING
  },
  userId: {
    type: Sequelize.UUID
  }
}

const ResellerSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  personal: {
    type: Sequelize.STRING // Name, home address,
  },
  official: {
    type: Sequelize.JSON   // shop/office Address, Phone, Email
  },
  kyc: {
    type: Sequelize.JSON
  }
}

module.exports = {
  SmeSchema,
  BrandSchema,
  ResellerSchema,
  TransactionSchema,
  AccountSchema,
  BankSchema,
  LoanSchema,
  LandSchema
};
