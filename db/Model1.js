
const Sequelize = require('sequelize');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './mydb.sqlite',
    logging: false
});

const {
  UserSchema,
  TransactionSchema,
  AccountSchema,
  BankSchema
} = require('./Schema');

const User = sequelize.define('user', UserSchema);
const Transaction = sequelize.define('transaction', TransactionSchema);
const Bank = sequelize.define('bank', BankSchema);
const Account = sequelize.define('account', AccountSchema);

User.hasMany(Transaction);
User.hasMany(Account);
User.hasMany(Bank);
Account.hasOne(Transaction);
Bank.hasOne(Transaction);

sequelize.sync();//{force: true});

module.exports = {
  User,
  Transaction,
  Bank,
  Account,
  Sequelize
};
