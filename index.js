var express = require('express');
const https = require('https');
const http = require('http');
const fs = require('fs');
const app = express();
const morgan = require('morgan');
const config = require('dotenv').config();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var cors = require('cors');
var utils = require('./utils');
const PORT = process.env.PORT || 3000;

// Certificate
if(process.env.ENV_TYPE != "DEV"){
	const privateKey = fs.readFileSync('/etc/letsencrypt/live/api.bahikhata.org/privkey.pem', 'utf8');
	const certificate = fs.readFileSync('/etc/letsencrypt/live/api.bahikhata.org/cert.pem', 'utf8');
	const ca = fs.readFileSync('/etc/letsencrypt/live/api.bahikhata.org/chain.pem', 'utf8');

	const credentials = {
		key: privateKey,
		cert: certificate,
		ca: ca
	};

	const httpsServer = https.createServer(credentials, app);
	httpsServer.listen(PORT, (req,res) => {
	     console.log('server listening at : ',PORT);
	});
} else {
	const httpServer = http.createServer(app);
	httpServer.listen(PORT, (req,res) => {
		   console.log('server listening at : ',PORT);
	});
}

app.use(morgan('tiny'));
app.use(cookieParser());
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var { local1, local2, sync, auth, api } = require('./router');

//app.use('/v1', local1);
//app.use('/v2', local2);
app.use(express.static('public', { dotfiles: 'allow' }));
app.use('/sync', sync);
app.use('/auth', auth);
app.use('/v1', utils.token.verify(), api);
