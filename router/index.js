var local1 = require('./local1');
var local2 = require('./local2');
var sync = require('./sync');
var api = require('./api');
var auth = require('./auth');
//console.log(auth);

module.exports = {
  local1, local2, sync, api, auth
};
