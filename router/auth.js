var express = require('express');
const router = express.Router();
const uuid = require('uuid/v4');
const { Brand } = require('../db/Remote');
const utils = require('../utils');
const crypto = require('crypto');
const secret = process.env.SECRET;

router.post('/login', (req,res) => {
  const { email, password } = req.body;
  Brand.findOne({ where: { email: email } })
    .then((user)=> {
      if(!user) {
        res.status(400).send({result: "User doesn't exist"});
      }
      let hashedPassword = crypto.createHmac('sha256', secret).update(password).digest('hex');
      if(user.password == hashedPassword) {
        if(user.emailVerified){
          let authToken = utils.token.sign(email);
          res.cookie('userId', user.id);
          res.status(200).send({
            success: true,
            message: 'Authentication Successful',
            token: authToken
          });
        } else {
          res.status(400).send({success: false, message: "Kindly verify email to login"});
        }
      } else {
        res.status(400).send({success: false, message: "Invalid Password"});
      }
    })
    .catch((err) => res.status(400).send({result: err}));
});

router.post('/register', (req,res) => {
  const { email, password } = req.body;
  //var obj = JSON.parse(keyContact);
  var hashedPassword = crypto.createHmac('sha256', secret).update(password).digest('hex');
  var emailToken = crypto.randomBytes(16).toString('hex');
  Brand.create({id: uuid(), email: email, password: hashedPassword, token: emailToken})
    .then((user) => {
      console.log(user);
      if(user) {
        var options = {
          verify: process.env.URL + "/auth/verify/" + emailToken
        };
        utils.mail.sendMail(email, "d-a6c3f4163fb34acc9b99c7c27c529b83", options);
        res.status(200).send({result: "Registered Successfully. Check Email for Verfication."});
      } else {
        res.status(400).send({result: "Registration Failed !"});
      }
    })
    .catch((err) => {
      if(err.name == "SequelizeUniqueConstraintError") {
        res.status(400).send({result: "User Account Exists"});
      } else if(err.name == "SequelizeHostNotFoundError") {
        res.status(400).send({result: "Server not Reachable"});
      } else {
        res.status(400).send(err);
      }
    });
});

router.post('/forgot', (req,res) => {
  const { email } = req.body;
  var emailToken = crypto.randomBytes(16).toString('hex');
  var options = {
    verify: process.env.URL + "/auth/verify/" + emailToken
  };

  utils.mail.sendMail(email, "d-22686adb40664e4d94d7cd7246c9708a", options);
  res.send({result: 'Kindly check Email for password reset Instructions.'});
});

router.get('/verify/:token', (req,res) => {
  const { token } = req.params;
  Brand.findOne({ where: { token: token }})
    .then((user)=> {
      if(!user) res.send({result: "Invalid Token"});
      else {
        Brand.update({ emailVerified: true, token: null }, { where: { id: user.id }})
        .then((user) => {
          if(user){
            res.send({result: "Email Successfully Verified !! Continue to <a href='/login'>login</a>"});
          }
        });
      }
    });
});


module.exports = router;
