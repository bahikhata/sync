var express = require('express');
const router = express.Router();
const uuid = require('uuid/v4');
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });
const { Loan, Land, Brand } = require('../db/Remote');
var utils = require('../utils');

/*router.get('/loan', (req,res) => {
  //let userId = req.cookies.userId;
  Loan.findAll()
    .then((loans) => {
      res.send(loans);
    }).catch((err) => res.send(err));
});

router.post('/loan', (req,res) => {
  const { amount, business, productChoice } = req.body;

  Loan.create({id: uuid(), amount: amount, business: business, productChoice: productChoice})
    .then((loan)=> {
      res.status(200).send(loan);
    }).catch((err) => {
      if(err.name == "SequelizeHostNotFoundError") {
        res.status(400).send({result: "Server not Reachable"});
      } else {
        res.status(400).send(err);
      }
    });

});*/

router.post('/land', upload.single('media'), (req,res) => {
  const { location, name, size } = req.body;
  const file = req.file;
  utils.s3.upload(file).then((result) => {
    if(result){
      Land.create({ id: uuid(), location: location, name: name, size: size, media: result }).then((land) => {
        res.status(200).send(land);
      }).catch(err => {
        if(err.name == "SequelizeHostNotFoundError") {
          res.status(400).send({result: "Server not Reachable"});
        } else {
          res.status(400).send(err);
        }
      });
    }
  }).catch((err) => {
    console.log(err);
  });
});

router.get('/land', (req,res) => {
  Land.findAll()
    .then((lands) => {
      res.send(lands);
    }).catch((err) => res.send(err));
});

router.get('/profile', (req,res) => {
  let userId = req.cookies.userId;
  Brand.findOne({ where: { id: userId }})
    .then((user)=> {
      res.send(user);
    }).catch((err) => res.send(err));
});

router.put('/profile', (req,res) => {
  const { keyContact } = req.body;
  //var obj = JSON.parse(keyContact);
  let userId = req.cookies.userId;
  Brand.update({ keyContact: keyContact }, { where: { id: userId }})
    .then((user)=> {
      res.send(user);
    }).catch((err) => res.send(err));
});

router.post('/logout', (req,res) => {
  res.clearCookie('userId');
  res.status(200).send('Logged Out');
  //res.redirect('http://localhost:4000');
});

module.exports = router;
